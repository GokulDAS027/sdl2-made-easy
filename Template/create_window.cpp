/*
 * Create Window
 * SDL2 window creation template.
 */
#include <SDL2/SDL.h>

#include <iostream>

int main() {
  SDL_Window *window = nullptr;

  bool isRunning = true;
  SDL_Event ev;

  // check if SDL2 is success
  if (SDL_Init(SDL_INIT_VIDEO) < 0) {
    std::cout << "Video window creation error" << SDL_GetError() << std::endl;
  } else {
    // SDL2 init success
    // create window
    window =
        SDL_CreateWindow("title: SDL2 made easy", SDL_WINDOWPOS_CENTERED,
                         SDL_WINDOWPOS_CENTERED, 640, 480, SDL_WINDOW_SHOWN);

    // check if window creation failed
    if (window == NULL) {
      std::cout << "Window creation failed" << SDL_GetError() << std::endl;
    } else {
      // window creation success
      while (isRunning) {
        // check poll events (keyboard inputs, mouse events...)
        while (SDL_PollEvent(&ev) != 0) {
          // quit if poll event asks for quitting
          if (ev.type == SDL_QUIT) {
            isRunning = false;
          } else if (ev.type == SDL_KEYDOWN) {
            if (ev.key.keysym.sym == SDLK_ESCAPE) {
              isRunning = false;
            }
          } else if (ev.type == SDL_MOUSEBUTTONDOWN) {
            if (ev.button.button == SDL_BUTTON_LEFT) {
              isRunning = false;
            }
          }
        }

        SDL_UpdateWindowSurface(window);
      }
    }
  }

  SDL_DestroyWindow(window);
  SDL_Quit();

  return 0;
}
