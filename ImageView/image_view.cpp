/*
 * Image View
 * Create a window and render image.
 */
#include <SDL2/SDL.h>

#include <iostream>

int imageView() {
  SDL_Window *window = nullptr;
  SDL_Surface *windowSurface = nullptr;
  SDL_Surface *imageSurface = nullptr;

  bool isRunning = true;
  SDL_Event ev;

  // check if SDL2 is success
  if (SDL_Init(SDL_INIT_VIDEO) < 0) {
    std::cout << "Video window creation error" << SDL_GetError() << std::endl;
  } else {
    // SDL2 init success
    // create window
    window =
        SDL_CreateWindow("title: SDL2 made easy", SDL_WINDOWPOS_CENTERED,
                         SDL_WINDOWPOS_CENTERED, 640, 480, SDL_WINDOW_SHOWN);

    // check if window creation failed
    if (window == NULL) {
      std::cout << "Window creation failed" << SDL_GetError() << std::endl;
    } else {
      // window creation success
      // create windowSurface and imageSurface
      windowSurface = SDL_GetWindowSurface(window);
      imageSurface = SDL_LoadBMP("../ImageView/assets/image.bmp");

      // check if image surface created
      if (imageSurface == NULL) {
        std::cout << "Image loading failed " << SDL_GetError() << std::endl;
      } else {
        // imageSurface creation success
        // draw imageSurface onto windowSurface
        SDL_BlitSurface(imageSurface, NULL, windowSurface, NULL);

        // gameloop
        while (isRunning) {
          // check poll events (keyboard inputs, mouse events...)
          while (SDL_PollEvent(&ev) != 0) {
            // quit if poll event asks for quitting
            if (ev.type == SDL_QUIT) {
              isRunning = false;
            } else if (ev.type == SDL_KEYDOWN) {
              if (ev.key.keysym.sym == SDLK_ESCAPE) {
                isRunning = false;
              }
            } else if (ev.type == SDL_MOUSEBUTTONDOWN) {
              if (ev.button.button == SDL_BUTTON_LEFT) {
                isRunning = false;
              }
            }
          }

          SDL_UpdateWindowSurface(window);
        }
      }
    }
  }

  // free image surface resources
  // windowSurface is destroyed with DestroyWindow
  SDL_FreeSurface(imageSurface);
  imageSurface = nullptr;

  SDL_DestroyWindow(window);
  windowSurface = nullptr;
  window = nullptr;

  SDL_Quit();

  return 0;
}
