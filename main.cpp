#include <iostream>

#include "ImageView/ImageView.h"

int main() {
  uint choice;

  std::cout << "SDL Demo Projects: " << std::endl;
  std::cout << "1 : ImageView" << std::endl;
  std::cout << "Choose : ";
  std::cin >> choice;

  switch (choice) {
    case 1:
      imageView();
      break;

    default:
      std::cout << "\nInvalid Choice" << std::endl;
      break;
  }

  return 0;
}
