cmake_minimum_required(VERSION 3.19)

# project name and version
project(SDL2MadeEasy VERSION 1.0)

include_directories(${PROJECT_SOURCE_DIR}/ImageView)

# specify compile commands
SET(SDL2_LINK_FLAGS    "-lSDL2")
SET(CMAKE_EXE_LINKER_FLAGS  "${CMAKE_EXE_LINKER_FLAGS} ${SDL2_LINK_FLAGS}")

# specify the C++ standard
set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED True)
set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

# executable
add_executable(SDL2MadeEasy main.cpp ImageView/image_view.cpp)

target_include_directories(SDL2MadeEasy PUBLIC "${PROJECT_BINARY_DIR}")